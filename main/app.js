function distance(first, second){
	if( !(Array.isArray(first)) || !(Array.isArray(second)) ) {
		throw new Error('InvalidType')
	}

	if(first.length==0 && second.length==0)
	{
		return 0
	}

	let firstUnique = first.filter(x => !(second.includes(x)))
	let secondUnique = second.filter(x => !(first.includes(x)))

	let uniqueItems = firstUnique.concat(secondUnique)
	uniqueItems = new Set(uniqueItems)

	let dist = uniqueItems.size

	return dist
}


module.exports.distance = distance